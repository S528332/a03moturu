QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    
    assert.strictEqual(bonus(2), 2, 'salary ');
    assert.strictEqual(bonus(-4), -4, 'negative salary Given');
    assert.throws(function () { bonus(); }, new Error("Expect numbers only"), 'need to give salary');
    assert.throws(function () { bonus("asd"); }, new Error("Expect numbers only"), 'need to give salary');
});
