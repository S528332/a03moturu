QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    assert.throws(function () { billing(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(billing(2,4), 8, 'All positive numbers');
    assert.strictEqual(billing(3,-6), -18, 'Positive and negative numbers');
    assert.strictEqual(billing(-4,-8), 32, 'All are negative numbers');
});
